<h1>Функция include()</h1>
<a href="/">Вернуться главную страницу</a>
<p>Функция подключает и выполняет (дополняя, в случае с .php файлами) содержимое указанного файла в месте того файла, где она была вызвана. Она не возвращает конечный результат выполнения скрипта другого файла при подключении, а только дополняет исполняемый скрипт текущего файла.</p>

<?php
  include('example-file-1.php');
  include('example-file-2.php');
?>