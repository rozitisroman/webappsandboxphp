<h1>Глобальная переменная $_POST</h1>
<a href="/">Вернуться главную страницу</a>
<p>Переменная является ассоциативным массивом данных, получаемым сервером через POST-запрос.</p>


<?php if($_SERVER['REQUEST_METHOD'] == 'POST'){ ?>
  <h2>Массив $_POST на коде</h2>
  <code>
    <?php print_r($_POST); ?>
  </code>
<?php } ?>

<h2>Отправить серверу POST-запрос можно с помощью формы</h2>
<form method="post">
  <p><input type="text" name="lol" value="LOL"></p>
  <p><input type="text" name="kek" value="KEK"></p>
  <p><input type="text" name="che" value="CHE"></p>
  <button type="submit">Отправить данные формы</button>
</form>

<p>
  Вызов свойств производится методо $_POST['название свойства']
</p>