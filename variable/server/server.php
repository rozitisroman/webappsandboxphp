<h1>Глобальная переменная $_SERVER</h1>
<a href="/">Вернуться главную страницу</a>
<p>Переменная является ассоциативным массивом данных о сервере, клиенте и хосте.</p>



<h2>Массив $_SERVER на коде</h2>
<code>
  <?php print_r($_SERVER); ?>
</code>



<h2>Свойства (или индексы) массива $_SERVER</h2>

<h3>Свойство HTTP_HOST</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['HTTP_HOST']; ?>
</p>

<h3>Свойство HTTP_REFERER</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['HTTP_REFERER']; ?>
</p>

<h3>Свойство PATH</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['PATH']; ?>
</p>

<h3>Свойство HTTP_USER_AGENT</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['HTTP_USER_AGENT']; ?>
</p>

<h3>Свойство SERVER_SOFTWARE</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['SERVER_SOFTWARE']; ?>
</p>

<h3>Свойство SERVER_NAME</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['SERVER_NAME']; ?>
</p>

<h3>Свойство SERVER_ADDR</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['SERVER_ADDR']; ?>
</p>

<h3>Свойство SERVER_PORT</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['SERVER_PORT']; ?>
</p>

<h3>Свойство DOCUMENT_ROOT</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['DOCUMENT_ROOT']; ?>
</p>

<h3>Свойство REQUEST_METHOD</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['REQUEST_METHOD']; ?>
</p>

<h3>Свойство REQUEST_URI</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['REQUEST_URI']; ?>
</p>

<h3>Свойство SCRIPT_NAME</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['SCRIPT_NAME']; ?>
</p>

<h3>Свойство PHP_SELF</h3>
<p>
  Результат вывода свойства: 
  <?php echo $_SERVER['PHP_SELF']; ?>
</p>