<h1>Глобальная переменная $_GET</h1>
<a href="/">Вернуться главную страницу</a>
<p>Переменная является ассоциативным массивом данных, получаемым сервером через GET-запрос.</p>



<h2>Массив $_GET на коде</h2>
<code>
  <?php print_r($_GET); ?>
</code>

<h2>Свойства массива зависят от запроса к серверу</h2>
<p>
  <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?lol=LOL">Обновить адрес и добавить поле запроса "lol"</a>
  <br>
  <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?kek=KEK">Обновить адрес и добавить поле запроса "kek"</a>
  <br>
  <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?che=CHE">Обновить адрес и добавить поле запроса "che"</a>
</p>
<p>
  <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?lol=LOL&kek=KEK">Обновить адрес и добавить поля запроса "lol" + "kek"</a>
  <br>
  <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?kek=KEK&che=CHE">Обновить адрес и добавить поля запроса "kek" + "che"</a>
  <br>
  <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?che=CHE&lol=LOL">Обновить адрес и добавить поля запроса "che" + "lol"</a>
</p>
<p>
  Вызов свойств производится методо $_GET['название свойства']
</p>