<h1>Песочница курса веб-приложения</h1>

<h2>Глобальные переменные</h2>
<ul>
  <li><a href="/variable/server/server.php">$_SERVER</a></li>
  <li><a href="/variable/get/get.php">$_GET</a></li>
  <li><a href="/variable/post/post.php">$_POST</a></li>
  <li><a href="/variable/request/request.php">$_REQUEST</a></li>
</ul>

<h2>Функции</h2>
<ul>
  <li><a href="/function/include/include.php">include()</a></li>
  <li><a href="/function/require/require.php">require()</a></li>
</ul>